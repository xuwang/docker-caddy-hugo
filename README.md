# Docker Example - A super fast static web with [Caddy] and [Hugo]

[Caddy] is the HTTP/2 web server with automatic HTTPS. Github stars 11,994.

[Hugo] builds static web site easy and fast, really really fast. Github starts: 16746

[Caddy] + [Hugo] + [Docker] = Super fast static web site that go anywhere.

## Prerequisites

#### Add test site name to /etc/hosts or your DNS server

E.g. the test site FQDN is _docker-example.somdev.stanford.edu_

```
echo "127.0.0.1 docker-example.somdev.stanford.edu" >> /etc/hosts
```

#### Optional [hugo]

If you need to do hugo dev on local host, you may need hugo installed:

```
$ brew install hugo
```

## Build the caddy-hugo docker image

```
$ make build
```

## Bring it up

```
$ make web
```

## Open hugo admin console

```
$ make admin
```

## Working on the website source

Don't knwo [hugo]? No problem, here is the [Quick Start], which this demo is based on.

If known [hugo] already, then edit files in srv/site and reload page on the brower.

Or just make changes through hugo's admin console, login with admin/admin for this demo.

## Shut it down

```
$ make down
```

[Caddy]: https://caddyserver.com/
[Hugo]: https://gohugo.io/
[Docker]: https://www.docker.com/
[Quick Start]: https://gohugo.io/overview/quickstart/
